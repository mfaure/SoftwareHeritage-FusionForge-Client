# SoftwareHeritage FusionForge plugin

## Description

This plugin provides a forge-wide view into the activities of all
projects (modulo permissions).  It can be seen as an aggregate of all
the project-wide "activity" pages.

The relevant data is also made available through a SOAP API.

## SoftwareHeritage-FusionForge-Client.py

`SoftwareHeritage-FusionForge-Client.py` is a Python2 script meant to test whether a FusionForge instance with SoftwareHeritage plugin installed is actually working (or not :) )

(It was created for testing http://adullact.net/)

As FusionForge offers a SOAP API, SoftwareHeritage is called with a SOAP client. You may use a desktop tool like [SoapUI](https://www.soapui.org/) and make requests manually, or use SoftwareHeritage-FusionForge-Client, which is a Python script using [SUDS library](https://fedorahosted.org/suds/), a lightweight SOAP python client. (One day, it could be updated with [Zeep library](http://docs.python-zeep.org/en/latest/) which seems much more supported.)

### Pre-requisites

```sh
sudo apt-get install python-suds
```

### Usage

Note: this is Python2

Edit and adjust for your needs.

```sh
python2 SoftwareHeritage-FusionForge-Client.py
```