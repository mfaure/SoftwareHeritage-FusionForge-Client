# GlobalActivity plugin

## Description

This plugin provides a forge-wide view into the activities of all
projects (modulo permissions).  It can be seen as an aggregate of all
the project-wide "activity" pages.

The relevant data is also made available through a SOAP API.

## Usage of the API

The method is called `globalactivity_getActivity`.  It takes four
parameters.

### Session token

A session token (as for the rest of the SOAP API): string (as
returned from the login method)
Possibly empty if the API is to be used anonymously.

### Start date

A start date: integer (Unix timestamp: number of seconds since 1970-01-01 00:00:00 UTC)

### End date

An end date: integer (ditto);

### List of sections

A list of sections for which to list events (array of strings), to be picked among:

* scmsvn,
* svngit,
* svnhg,
* trackeropen,
* trackerclose,
* frsrelease,
* forumpost,
* news,
* taskopen,
* taskclose,
* taskdelete,
* docmannew,
* docmanupdate,
* docgroupnew.

An empty list means no filtering.

## Return value

The return value is an array of _GlobalActivityEntry_. A
_GlobalActivityEntry_ is a hash with the following keys:

* `group_id` identifier for the project on the forge,
* `section` see above, with a quirk: scmsvn, scmgit and scmhg all end up in the same "scm" section,
* `ref_id` meaning differs across sections,
* `subref_id` ditto,
* `description` title, commit number, or otherwise descriptive text,
* `activity_date` as a Unix timestamp.

## Global Activity per project

Despite its name, the plugin also provides a non-global SOAP method
called `globalactivity_getActivityForProject`, which takes a `group_id` as
an extra parameter between the end date and the list of sections.  It
returns an array of _GlobalActivityEntry_ that only concerns the
requested project.

URL: https://adullact.net/plugins/globalactivity/

## Example

"Test-GlobalActivity" is a Python script meant to test whether a FusionForge instance with plugin GlobalActivity installed is actually working (or not :) )

(It was created for testing http://adullact.net/)

As FusionForge offers a SOAP API, GlobalActivity is called with a SOAP client. You may use a desktop tool like [SoapUI](https://www.soapui.org/) and make requests manually, or use Test-GlobalActivity, which is a Python script using [SUDS library](https://fedorahosted.org/suds/), a lightweight SOAP python client. (One day, it could be updated with [Zeep library](http://docs.python-zeep.org/en/latest/) which seems much more supported.)

### Pre-requisites

```sh
sudo apt-get install python-suds
```

### Usage

Note: this is Python2

```sh
python2 test-globalactivity.py
```